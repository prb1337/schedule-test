# Schedule Test

## Architektur
### Die Facade
Die Hauptlogik steckt in der ``ScheduleTestFacade``, die nur eine simple Schnittstelle für den Aufrufer zur Verfügung stellt,
in dem Fall die Main-Klasse. Für die Initalisierung erhält die Facade eine Implementierung für den TestSystemService, 
was hier eine Mockimplentierung ist, später aber eine Implementierung mit einer richtigen Datenbankanbindung sein kann.

### Datenbank Zugriff
Für den Datenbankzugriff gibt es zwei Methoden (siehe auch ``DatabaseInterface``):

``Optional<TestSuite> findByName(String name) ``  
Liefert die TestSuite mit den dazugehörigen Betriebssystemen und Hardware für einen Namen

``List<TestSystem> findTestSystemsByOsAndHardware(List<OperatingSystem> os, List<Hardware> hardware)``  
Liefert eine Liste aller Testsysteme, die die übergebenen Betriebssysteme und Hardware unterstützt

### TestSystemTrigger
Die Klasse ``TestSystemTrigger`` stellt die Schnittstelle zum eigentlichen Testsystem dar. Sie kann überprüfen, ob das System derzeit 
beschäftigt ist und bei Verfügbarkeit den Job starten. Als Erweiterung kann diese Klasse in ein Template umgewandelt werden, um in Zukunft verschiedene Clients
verwenden zu können, z.B. REST oder ein Messaging Mechanismus für einen asynchronen Ansatz.


## Tests
Für die einfachen Unit Tests sollten die Datenbankzugriffe und die TestSystemTrigger-Responses gemockt werden.
Dadurch kann die Facade, was die meiste Logik enthält, schon komplett abgetestet werden, z.B. was passiert wenn ein unbekannter TestSuite-Name übergeben wird,
wird ein Eintrag in die Retry-Liste geschrieben, wenn alle Testsysteme beschäftigt sind, etc.

Für den Integrationstest kann zum Teststart eine H2-InMemory Datenbank gestartet werden, die mit Beispieldaten befüllt wird.
Damit wäre sichergestellt, dass die Tests immer mit den gleichen Daten ausgeführt und vor allem, dass der Zugriff auf die Datenbank funktioniert.  
Die Nachricht, die ``TestSystemTrigger`` an das Testsystem abschickt, sollte abgefangen und überprüft werden. Eventuell bietet sich
hier ein Contract Test an, um sicherzustellen, dass die Nachricht noch immer dem Format entspricht, das der Abnehmer erwartet.




## Die Anwendung lokal starten
Mit ``mvn package`` das Java Artefakt bauen.  

Beispiel Aufruf um die HomeSecuritySimple Testsuite zu starten:  
``java -Duser.email=patrick@rebmann.de -jar target\schedule-test-1.0-SNAPSHOT.jar HomeSecuritySimple``

In diesem Demo Projekt gibt es nur die HomeSecuritySimple Testsuite, bei einem anderen Wert kommt es zum Fehler.
Durch einen Zufallsmechanismus gibt es eine 50% Wahrscheinlichkeit, dass das Testsystem nicht beschäftigt ist.