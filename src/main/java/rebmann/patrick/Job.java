package rebmann.patrick;

import java.util.Random;

public class Job {

    public boolean isRunning() {
        return new Random().nextInt(2) != 1;
    }

    public void sendResults(String email) {
        System.out.println("sending email to " + email);
    }

}
