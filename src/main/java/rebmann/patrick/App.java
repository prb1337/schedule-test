package rebmann.patrick;


import rebmann.patrick.service.TestSystemService;
import rebmann.patrick.service.MockTestSystemService;
import rebmann.patrick.service.ScheduleTestFacade;

public class App {
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.err.println("No args given");
        }

        TestSystemService systemServiceStrategy = new MockTestSystemService();
        ScheduleTestFacade facade = new ScheduleTestFacade(systemServiceStrategy);
        facade.startScheduleTest(args[0]);
    }
}
