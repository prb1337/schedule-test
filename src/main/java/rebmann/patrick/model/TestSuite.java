package rebmann.patrick.model;

import java.util.List;


/**
 * Database entity for the Test Suite. Contains the name of the test suite and a list of corresponding hardware and operating systems
 */
public class TestSuite {

    private long id;

    private String name;

    // @ManyToMany n:m relation
    private List<Hardware> hardware;

    // @ManyToMany n:m relation
    private List<OperatingSystem> operatingSystems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Hardware> getHardware() {
        return hardware;
    }

    public void setHardware(List<Hardware> hardware) {
        this.hardware = hardware;
    }

    public List<OperatingSystem> getOperatingSystems() {
        return operatingSystems;
    }

    public void setOperatingSystems(List<OperatingSystem> operatingSystems) {
        this.operatingSystems = operatingSystems;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
