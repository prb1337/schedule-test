package rebmann.patrick.service;

import rebmann.patrick.model.Hardware;
import rebmann.patrick.model.OperatingSystem;
import rebmann.patrick.model.TestSuite;
import rebmann.patrick.model.TestSystem;
import rebmann.patrick.service.database.DatabaseInterface;
import rebmann.patrick.service.database.MockDatabaseInterfaceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Mock service. "Connects" to a mock database
 */
public class MockTestSystemService extends TestSystemService {

    private DatabaseInterface dbInterface = new MockDatabaseInterfaceImpl();
    private List<String> retryList = new ArrayList<>();

    @Override
    public Optional<TestSuite> findTestSuiteByName(String testSuiteName) {
        return dbInterface.findByName(testSuiteName);
    }

    @Override
    public List<TestSystem> findTestSystemsByOSAndHardware(List<OperatingSystem> os, List<Hardware> hardware) {
        return dbInterface.findTestSystemsByOsAndHardware(os, hardware);
    }

    @Override
    protected void writeTestSuiteForRetry(String testSuiteName) {
        retryList.add(testSuiteName);
    }

    @Override
    protected List<String> getRetryTestSuites() {
        return retryList;
    }

}
