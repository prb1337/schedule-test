package rebmann.patrick.service;

import rebmann.patrick.model.Hardware;
import rebmann.patrick.model.OperatingSystem;
import rebmann.patrick.model.TestSuite;
import rebmann.patrick.model.TestSystem;

import java.util.List;
import java.util.Optional;


/**
 * Abstract class to find the compatible test system for a test suite name. Provides a way to save the testsuites which could not be started.
 *
 * The extending classes can connect to different database endpoints.
 *
 */
public abstract class TestSystemService {

    public List<TestSystem> findCompatibleTestSystems(String testSuiteName) {
        Optional<TestSuite> optionalTestSuite = findTestSuiteByName(testSuiteName);

        // get test suite or throw exception if no testsuite was found
        TestSuite testSuite = optionalTestSuite.orElseThrow(() -> (new RuntimeException("No test suite found for " + testSuiteName)));

        List<TestSystem> testSystems = findTestSystemsByOSAndHardware(testSuite.getOperatingSystems(), testSuite.getHardware());
        if (testSystems.isEmpty()) {
            throw new RuntimeException("no test system found for given hardware and os");
        }

        return testSystems;
    }

    protected abstract Optional<TestSuite> findTestSuiteByName(String testSuiteName);

    protected abstract List<TestSystem> findTestSystemsByOSAndHardware(List<OperatingSystem> os, List<Hardware> hardware);

    protected abstract void writeTestSuiteForRetry(String testSuiteName);

    protected abstract List<String> getRetryTestSuites();

}
