package rebmann.patrick.service;

import rebmann.patrick.Job;
import rebmann.patrick.model.TestSystem;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ScheduleTestFacade {

    private TestSystemService schedule;

    public ScheduleTestFacade(TestSystemService template) {
        this.schedule = template;

        // initialize retry mechanism
        this.initializeRetryMechanism();
    }

    public void startScheduleTest(String testSuiteName) {
        List<TestSystem> testSystems = schedule.findCompatibleTestSystems(testSuiteName);
        if (testSystems.isEmpty()) {
            throw new RuntimeException("no test system found for given hardware and os");
        }

        // create TestSystemtrigger objects
        List<TestSystemTrigger> testSystemTriggers = testSystems.stream().map(TestSystemTrigger::new).collect(Collectors.toList());

        // find first idle test system
        Optional<TestSystemTrigger> testSystemTrigger = testSystemTriggers.stream().filter(TestSystemTrigger::isIdle).findFirst();

        // no idle test system were found
        if (!testSystemTrigger.isPresent()) {
            System.err.println("no idle test system found, write testSuite " + testSuiteName + " into DB for retry");
            schedule.writeTestSuiteForRetry(testSuiteName);
            return;
        }

        Job job = testSystemTrigger.get().startSuite(testSuiteName);
        System.out.println("Testjob is started");
        do {
            System.out.println("Job is still running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        } while (job.isRunning());
        System.out.println("Job is done");
        String userMail = System.getProperty("user.email");
        job.sendResults(userMail);

    }


    /**
     * Initialize a retry mechanism, e.g. a cron job
     */
    private void initializeRetryMechanism() {
        // get the list of test suite waiting a retry
        List<String> retries = schedule.getRetryTestSuites();

        for (String testSuite : retries) {
            // TODO implemantation for retry
        }
    }


}
