package rebmann.patrick.service.database;

import rebmann.patrick.model.Hardware;
import rebmann.patrick.model.OperatingSystem;
import rebmann.patrick.model.TestSuite;
import rebmann.patrick.model.TestSystem;

import java.util.List;
import java.util.Optional;


/**
 * Interface for Database
 */
public interface DatabaseInterface {

    /**
     *
     * @param name of the test suite
     * @return a optional of TestSuite, depending on an existing entry for the given name
     */
    Optional<TestSuite> findByName(String name); // e.g. name =

    /**
     * Find the list of possible test systems depending on the given operating systems and hardware
     * @param os list of operating systems
     * @param hardware list of hardware
     * @return the list of test systems
     */
    List<TestSystem> findTestSystemsByOsAndHardware(List<OperatingSystem> os, List<Hardware> hardware);

}
