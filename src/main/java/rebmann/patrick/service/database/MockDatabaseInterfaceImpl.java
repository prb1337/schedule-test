package rebmann.patrick.service.database;

import rebmann.patrick.model.Hardware;
import rebmann.patrick.model.OperatingSystem;
import rebmann.patrick.model.TestSuite;
import rebmann.patrick.model.TestSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MockDatabaseInterfaceImpl implements DatabaseInterface {

    @Override
    public Optional<TestSuite> findByName(String name) {
        if (!name.equalsIgnoreCase("HomeSecuritySimple")) {
            return Optional.empty();
        }
        TestSuite testSuite = new TestSuite();

        testSuite.setName("HomeSecuritySimple");

        List<OperatingSystem> osList = new ArrayList<>();
        osList.add(new OperatingSystem(1, "Windows 7"));
        osList.add(new OperatingSystem(2, "Windows 10"));
        testSuite.setOperatingSystems(osList);

        List<Hardware> hardwareList = new ArrayList<>();
        hardwareList.add(new Hardware(1, "electronic lock"));
        hardwareList.add(new Hardware(2, "camera"));
        testSuite.setHardware(hardwareList);

        return Optional.of(testSuite);
    }

    @Override
    public List<TestSystem> findTestSystemsByOsAndHardware(List<OperatingSystem> os, List<Hardware> hardware) {
        if (os == null || os.isEmpty() || hardware == null || hardware.isEmpty()) {
            System.err.println("findTestSystemsByOsAndHardware returned empty list, no OS or hardware were provided");
            return new ArrayList<>();
        }

        List<TestSystem> result = new ArrayList<>();

        TestSystem testSystem = new TestSystem();
        List<OperatingSystem> osList = new ArrayList<>();
        osList.add(new OperatingSystem(1, "Windows 7"));
        osList.add(new OperatingSystem(2, "Windows 10"));
        testSystem.setOperatingSystems(osList);

        List<Hardware> hardwareList = new ArrayList<>();
        hardwareList.add(new Hardware(1, "electronic lock"));
        hardwareList.add(new Hardware(2, "camera"));
        testSystem.setHardware(hardwareList);

        testSystem.setName("Jupiter");

        result.add(testSystem);

        return result;
    }
}
