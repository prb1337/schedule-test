package rebmann.patrick.service;

import rebmann.patrick.Job;
import rebmann.patrick.model.TestSystem;

import java.util.Random;

/**
 * Service class which has access to the test system. Can check for status of the system and start the test suite.
 */
public class TestSystemTrigger {

    private TestSystem testSystem;

    public TestSystemTrigger(TestSystem testSystem) {
        this.testSystem = testSystem;
    }

    public boolean isBusy() {
        return new Random().nextInt(2) == 1;
    }

    public boolean isIdle(){
        return !isBusy();
    }

    public Job startSuite(String testSuiteName) {
        System.out.println("starting test suite " + testSuiteName + " on system: " + testSystem.getName());
        return new Job();
    }
}
